## Installation

Clone project

```bash
git clone git@github.com:JanisRuska/handshake.git
```

Go to project folder

```bash
cd handshake
```

### Back end

Install composer dependencies

```bash
composer install
```

Copy .env.example

```bash
cp .env.example .env
```

Generate APP_KEY

```bash
php artisan key:generate
```

Create storage link

```bash
php artisan storage:link
```

Create Database and put configs into **.env** file

```dotenv
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=bicycle
DB_USERNAME=root
DB_PASSWORD=
```

Run migrations

```bash
php artisan migrate
```

Run seeders

```bash
php artisan db:seed
```

#### Front end

Install dependencies

```bash
npm install
```

Build front end (recommended do this on separate terminal)

```bash
npm run dev
```
