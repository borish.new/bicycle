<x-app-layout>
    <div class="w-full p-8 m-1.5">
        <div class="flex">
            <h3 class="py-1.5 text-4xl text-blue-900 font-extrabold ml-24">International Cycling Community of
                Practice</h3>
            <div class="w-[858px] h-[405px] mr-32">
                <img class="bg-gradient-to-t" src="{{asset('img/Asset2.jpg')}}">
            </div>
        </div>
        <div class="mb-5 flex items-center">
            <div class="block-1 border-2 w-[248px] h-[298px] m-[38px] ml-72 bg-sky-500 rounded-xl">
                <p class="pt-14 pl-2 text-xs text-white-50">Self-assessment</p>
                <p class="text-xs p-2 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum fermentum, montes, congue faucibus et mattis donec sed pretium.</p>
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50"> Start here </a>
            </div>
            <div class="block-2 border-2 w-[248px] h-[298px] m-[38px] bg-white rounded-xl">
                <p class="pt-14 pl-2 text-xs text-black">Database</p>
                <p class="text-xs p-2 text-black-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum fermentum, montes, congue faucibus et mattis donec sed pretium.</p>
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50"> View here </a>
            </div>
            <div class="block-3 border-2 w-[248px] h-[298px] m-[38px] bg-white rounded-xl">
                <p class="pt-14 pl-2 text-xs text-white-50">Support</p>
                <p class="text-xs p-2 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum fermentum, montes, congue faucibus et mattis donec sed pretium.</p>
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50"> View here </a>
            </div>
        </div>
    </div>
</x-app-layout>