@extends('layouts.survey')

@section('title-block')
    click-submit
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Click SUBMIT to finish</h3>
            <div class="mb-5 text-center">
                Thank you for completing this survey until the end. In less than 72 hours, you will receive the full
                report of your assessment at .

                This will inform you what your total score means and offer you guidance, advice and access to external
                materials to improve all the aspects of the cycling network of your city.

                By clicking Submit I agree to HANDSHAKE processing the data received through this survey for the tasks
                related to the elaboration of the self-assessment report.
            </div>
            <div class="ml-52">
                <img src="{{asset('img/image23.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex justify-between h-10">
            <div class="">
                <button class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black hover:bg-gray-50"> Previous
                </button>
            </div>
            <div class="flex justify-end">
                <button class="flex justify-end px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Submit
                </button>
            </div>
        </div>
    </div>
@endsection