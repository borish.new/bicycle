@extends('layouts.survey')

@section('title-block')
    your voice
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Your voice</h3>
            <div class="description py-2.5 text-sm text-center">
                Is there anything else you would like to add or tell us? Now is your final chance.
            </div>
            <div class="border-2 w-[571px] h-[185px] ml-[265px] rounded-md">
                <p class="mt-2 ml-2 text-xs">Something about myself.</p>
            </div>
            <div class="ml-52">
                <img src="{{asset('img/image22.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex">
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="#" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50 mr-96"> Previous </a>
            </div>
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="#" class="ml-3 relative inline-flex items-center px-16 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next </a>
            </div>
        </div>
    </div>
@endsection