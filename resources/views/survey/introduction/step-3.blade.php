@extends('layouts.survey')

@section('title-block')
    step-3
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">5 Levels of Perfomance</h3>
            <div class="description py-2.5 text-sm">
                In the first part, the level of performance of your city will be evaluated through three key areas:
                hardware,
                software and orgware. You will be asked to answer simple multiple-choice questions. Each response will
                contribute towards the overall assessment, which will be defined as one of the five Levels of
                Performance (Brass, Bronze, Silver, Gold and Platinum).
            </div>
            <div class="mb-5">
                This part will take you about 15 - 20 minutes to complete.
            </div>
            <div class="ml-52">
                <img src="{{asset('img/image17.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex">
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 2) }}" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50 mr-96"> Previous </a>
            </div>
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 4) }}" class="ml-3 relative inline-flex items-center px-16 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next </a>
            </div>
        </div>
    </div>
@endsection