@extends('layouts.survey')

@section('title-block')
    step-4
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">The second half</h3>
            <div class="description py-2.5 text-sm">
                The second half of the survey is about data collection.

                Some of the information asked here may require a bit of research. Depending on your knowledge, you may
                need
                from half an hour to a couple of hours to complete this part. In order to help you go smoothly through
                the
                questions, below you can see all the data you need to answer this part completely. With the data, you
                will
                need about 20 minutes to complete it.

                This part doesn't affect the results of Part 1. However, it will help analyse the context of cycling
                policy
                in your city and compare it to other cities in the Community of Practise platform. Therefore, although
                it's
                optional, it's highly recommended to respond it as completely as possible.
            </div>
            <div class="mb-5">
                DATA YOU NEED FOR THE SURVEY
            </div>
            <div class="ml-52">
                <img src="{{asset('img/image18.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex">
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 3) }}" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50 mr-96"> Previous </a>
            </div>
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 5) }}" class="ml-3 relative inline-flex items-center px-16 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next </a>
            </div>
        </div>
    </div>
@endsection