@extends('layouts.survey')

@section('title-block')
    step-7
@endsection

@section('content-block')
    @livewire('respondent-detalis', ['city' => $city ?? '', 'respondent' => $respondent ?? null])
@endsection
