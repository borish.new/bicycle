@extends('layouts.survey')

@section('title-block')
    step-1
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Perfomance and Assessment System for Cycling at
                All Levels</h3>
            <div class="description py-2.5">
                This project has received funding from the European Union‘s Horizon 2020 Research and Innovation
                programme under
                grant agreement no 769177. The sole responsibility for the content of this website lies with the CIVITAS
                Handshake
                project and in no way reflects the views of the European Commission. The UX of this survey has been
                mostly
                designed for a desktop screen.
            </div>
            <div class="ml-36">
                <img src="{{asset('img/image13.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16">
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 2) }}" class="ml-3 relative inline-flex items-center px-16 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-600 hover:bg-gray-50"> Next </a>
            </div>
        </div>
    </div>
@endsection