@extends('layouts.survey')

@section('title-block')
    step-2
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Structure of the Survey</h3>
            <div class="description py-2.5">
                This survey gives you the opportunity to assess and reflect on your city's cycling performance.
                It should be completed by an official representative on behalf of the city’s agreed viewpoint.
                Thus, you may want to gather collective feedback regarding cycling before starting. The following
                diagram shows how the survey is organised.
            </div>
            <div class="ml-36">
                <img src="{{asset('img/image16.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex">
            <div class="flex justify-between sm:justify-end">
                <a href="{{ route('steps', 1) }}" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50 mr-96"> Previous </a>
            </div>
            <div class="flex-1 flex justify-between sm:justify-end">
                <a href="{{ route('steps', 3) }}" class="ml-3 relative inline-flex items-center px-16 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50" > Next </a>
            </div>
        </div>
    </div>
@endsection