@extends('layouts.survey')

@section('title-block')
    step-6
@endsection

@section('content-block')
    @livewire('respondent-city', ['city' => $city ?? ''])
@endsection
