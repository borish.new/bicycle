@extends('layouts.survey')

@section('title-block')
    Question
@endsection

@section('content-block')
    @livewire('question', ['respondent' => $respondent, 'question' => $question])
@endsection
