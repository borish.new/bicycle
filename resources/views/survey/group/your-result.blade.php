@extends('layouts.survey')

@section('title-block')
    your-result
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Result - Your Current Level of Cycling
                Performance: SILVER 63/100</h3>
            <div class="ml-52">
                <img src="{{asset('img/image121.jpg')}}">
            </div>
            <div class="mb-5 text-center">
                Keep going! Out of the five stages of progress, we consider you have reached the following level of
                performance.
            </div>
        </div>
        <div class="paginate mt-16 flex justify-between h-10">
            <div class="">
                <a href="{{$previousPage}}" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black hover:bg-gray-50"> Previous
                </a>
            </div>
            <div class="sm:col-span-2 sm:flex sm:justify-center"></div>
            <div class="flex justify-end">
                <button class="flex justify-end px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next
                </button>
            </div>
        </div>
    </div>
@endsection