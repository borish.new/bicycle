@extends('layouts.survey')

@section('title-block')
    Group
@endsection

@section('content-block')
    <div class="w-full p-8 m-1.5">
    @if($group->id === 1)
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">In the first section we focus on infrastructure and facilities</h3>
            <div class="ml-52">
                <img src="{{asset('img/image20.jpg')}}">
            </div>
            <div class="mb-5 text-center">
                Please click NEXT to continue
            </div>
        </div>
    @elseif($group->id === 2)
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">In the second section we focus on communications and culture</h3>
            <div class="ml-52">
                <img src="{{asset('img/image21.jpg')}}">
            </div>
            <div class="mb-5 text-center">
                Please click NEXT to continue
            </div>
        </div>
    @else
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Section 3 Orgware</h3>
            <div class="ml-52">
                <img src="{{asset('img/image20.jpg')}}">
            </div>
            <div class="mb-5 text-center">
                Please click NEXT to continue
            </div>
        </div>
    @endif
        @livewire('group-component', ['group_id' => $group->id, 'respondent' => $respondent])
    </div>
@endsection
