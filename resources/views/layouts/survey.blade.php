<x-app-layout>
<div class="container mx-auto columns-2 flex flex-row">
    @livewire('sidebar', ['step' => $step ?? 7])
    @yield('content-block')
</div>
</x-app-layout>
