<x-app-layout>
    <div class="">
        <div class="bg-white w-[1440px] h-[115px] border-2 border-grin-500">
            @include('inc.header')
        </div>
        <div class="">
            @yield('block-content')
        </div>
    </div>
</x-app-layout>