<div>
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Are you a representant of your city?</h3>
            @error('city') <span class="error text-red-700">{{ $message }}</span> @enderror
            <div class="description py-2.5 text-sm">
                This survey has been designed to help public administrations. Please enter the email of your job position and
                the rest of the details in order to verify the legitimacy of your position. <b>The results of the survey will be
                    sent just to this email address.</b>
            </div>
            <div class="mb-5">
                <a href="#">Data will be held in compliance with GDPR regulations. View our privacy policy here.</a>
            </div>
            <div class="questions">
                <div class="py-4 px-4 sm:px-4 lg:col-span-2 xl:p-1">
                    <form action="#" method="POST" class="mt-6 grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8">
                        <div class="sm:col-span-2">
                            <label for="email" class="block text-sm font-medium text-warm-gray-900">Your email address</label>
                            <div class="mt-1">
                                <input type="text" wire:model="email" id="email" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                            </div>
                            @error('email') <span class="error text-red-700">{{ $message }}</span> @enderror
                        </div>
                        <div>
                            <label for="name" class="block text-sm font-medium text-warm-gray-900">Your name</label>
                            <div class="mt-1">
                                <input type="text" wire:model="name" id="name" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                            </div>
                            @error('name') <span class="error text-red-700">{{ $message }}</span> @enderror
                        </div>
                        <div>
                            <label for="your-position" class="block text-sm font-medium text-warm-gray-900">Your position/role in the organisation</label>
                            <div class="mt-1">
                                <input type="text" wire:model="role" id="your-position" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                            </div>
                            @error('role') <span class="error text-red-700">{{ $message }}</span> @enderror
                        </div>
                        <div>
                            <label for="name-of-authority" class="block text-sm font-medium text-warm-gray-900">Name of authority you are responding on behalf of</label>
                            <div class="mt-1">
                                <input id="name-of-authority" wire:model="company_name" type="text" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                            </div>
                            @error('company_name') <span class="error text-red-700">{{ $message }}</span> @enderror
                        </div>
                        <div>
                            <label for="country" class="block text-sm font-medium text-warm-gray-900">Country your organisation is based in</label>
                            <div class="mt-1">
                                <select wire:model="country_id" id="country" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                                    <option value="">Choose your country</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $country->id === $country_id ? 'selected' : '' }}>{{ $country->countryName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('country_id') <span class="error text-red-700">{{ $message }}</span> @enderror
                        </div>
                    </form>
                </div>
            </div>
            <div class="ml-52">
                <img src="{{asset('img/Asset1@2x 2.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex justify-between h-10">
            <div class="flex justify-start">
                <a href="{{ route('steps', ['step' => 6, 'city' => $city]) }}" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50"> Previous </a>
            </div>
            <div class="sm:col-span-2 sm:flex sm:justify-center">
                <button wire:click="saveAndContinueLater" type="submit" class="w-full inline-flex items-center justify-center px-6 py-3 border border-gray-300 rounded-md shadow-sm text-base font-medium text-base bg-gray-300/50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 sm:w-auto">Save & Continue Later</button>
            </div>
            <div class="flex justify-end">
                <button wire:click="createAndContinue" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next </button>
            </div>
        </div>
    </div>
</div>
