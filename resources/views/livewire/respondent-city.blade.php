<div>
    <div class="w-full p-8 m-1.5">
        <div class="h-5/6 bg-white p-3">
            <h3 class="text-center py-1.5 text-blue-900 font-extrabold">Name of your urban area</h3>
            <div class="description py-2.5 text-sm">
                Please first tell us the name of the area you are responding relating to.
            </div>
            <div class="mb-5">
                Name of area, e.g. local authority, city, town
            </div>
            <div class="w-[520px] h-[60px] mb-px">
                <input wire:model="city" type="text" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
            </div>
            @error('city') <span class="error text-red-700">{{ $message }}</span> @enderror
            <div class="mt-[10px]">
                <img src="{{asset('img/Asset1@2x 1.jpg')}}">
            </div>
        </div>
        <div class="paginate mt-16 flex justify-between h-10">
            <div class="flex justify-start">
                <a href="{{ route('steps', 5) }}" class="relative inline-flex items-center px-16 py-2 border border-gray-300 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50"> Previous </a>
            </div>
            <div class="sm:col-span-2 sm:flex sm:justify-center"></div>
            <div class="flex justify-end">
                <button wire:click="nextStep" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next </button>
            </div>
        </div>
    </div>
</div>
