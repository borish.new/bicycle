<div class="m-8">
    <h3 class="text-center py-2 text-blue-900 font-extrabold">{{ $question->title }}</h3>
    <div class="mb-5 py-2">{{ $question->subtitle }}</div>
    <div class="container mx-auto columns-1 p-8 border-2">
        @if($question->type === 'radio')
            <div class="grid" id="options">
                @foreach($question->options as $option)
                    <span class="checkmark"></span>
                    <label class="option h-16 my-1 text-left text-sm bg-slate-200 py-2.5 border-2">
                        <input type="radio" name="answer" wire:model="answer"
                               value="{{ $option->id }}"> {{ $option->value }}
                    </label>
                @endforeach
            </div>
        @elseif($question->type === 'checkbox')
            <div>checkbox logic</div>
        @else
            <div>input logic</div>
        @endif
        <div class="mx-auto columns-2 flex flex-row pt-4">
            <div class="text-xs border-2">
                Please tick a box if you can be asked to mentor colleagues in other cities on the specific element(s)
            </div>
            <div class="text-xs border-2">
                <input type="checkbox">
                I can give mentorship on this question
            </div>
        </div>
        <div class="paginate mt-16 flex justify-between h-10">
            <div class="flex justify-start">
                <button wire:click="goToPreviousQuestion" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black hover:bg-gray-50"> Previous
                </button>
            </div>
            <div class="sm:col-span-2 sm:flex sm:justify-center">
                @if ($this->answer)
                <button wire:click="saveAnswerAndContinueLater" type="submit"
                        class="w-full inline-flex items-center justify-center px-6 py-3 border border-gray-300 rounded-md shadow-sm text-base font-medium text-base bg-gray-300/50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500 sm:w-auto">
                    Save & Continue Later
                </button>
                @endif
            </div>
            <div class="flex justify-end">
                @if ($this->answer)
                <button wire:click="saveAnswerAndGoToNext" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300 text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50">
                    Next
                </button>
                @endif
            </div>
            @error('answer') <span class="error text-red-700">{{ $message }}</span> @enderror
        </div>
    </div>
</div>
