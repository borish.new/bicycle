<div class="">
    <nav class="space-y-1" aria-label="Sidebar">
        <h4 class="py-1.5 underline decoration-4 decoration-green-600">Introduction</h4>
        <a href="{{ route('steps', 1) }}"
           class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md
           {{ $step > 1 ? 'bg-sky-500/50' : 'white' }}">
            <span class="truncate"> Structure of the survey </span>
        </a>
        <a href="{{ route('steps', 2) }}"
           class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md
           {{ $step > 2 ? 'bg-sky-500/50' : 'white' }}">
            <span class="truncate"> 5 levels of perfomance </span>
        </a>
        <a href="{{ route('steps', 3) }}"
           class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md
           {{ $step > 3 ? 'bg-sky-500/50' : 'white' }}">
            <span class="truncate"> Data you need for the survey </span>
        </a>
        <div class="block-4 border-2 h-10 my-1 text-center text-sm py-2.5 border-blue-400
        {{ $step > 4 ? 'bg-sky-500' : 'white' }}">Let`s start!
        </div>
        <div class="part_1">
            <h4 class="py-2.5 text-sm underline decoration-4 decoration-green-600">PART 1: Level of perfomance</h4>
            @foreach($groups as $group)
                <div class="text-gray-600 text-center border-2 hover:bg-gray-50 hover:text-gray-900 px-3 mt-1 py-2 text-sm font-medium rounded-md">
                    {{ $group->title }}
                </div>
            @endforeach
            <div class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md">
                Your results
            </div>
        </div>
        <div class="part_2">
            <h5 class="py-2.5 text-sm underline decoration-4 decoration-green-600">Part 2: Data collection</h5>
            <div class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md">
                Section 1: City context
            </div>
            <div class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md">
                Section 2: Submission
            </div>
            <div class="text-gray-600 hover:bg-gray-50 hover:text-gray-900 flex items-center px-3 py-2 text-sm font-medium rounded-md">
                Your voice
            </div>
        </div>
    </nav>
</div>
