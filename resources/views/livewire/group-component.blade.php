<div class="paginate mt-16 flex justify-between h-10">
    <div class="">
        <button wire:click="goToPreviousQuestion" class="ml-3 relative inline-flex items-center px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black hover:bg-gray-50"> Previous
        </button>
    </div>
    <div class="sm:col-span-2 sm:flex sm:justify-center"></div>
    <div class="flex justify-end">
        <button wire:click="continueSurvey" class="flex justify-end px-14 py-2 border border-gray-300
            text-sm font-medium rounded-md text-black bg-sky-500 hover:bg-gray-50"> Next
        </button>
    </div>
</div>
