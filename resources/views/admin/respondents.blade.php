<x-app-layout>
    <div class="ml-6 mt-6 text-xl text-blue-900">All respondents</div>
    <div class="bg-white m-6 p-4 w-full">
        <div class="ml-6 mt-6">
            <table class="w-full">
                <thead>
                <tr class="border-2 h-12 bg-gray-300/50">
                    <th>id</th>
                    <th>uuid</th>
                    <th>user_id</th>
                    <th>email</th>
                    <th>city</th>
                    <th>name</th>
                    <th>role</th>
                    <th>company_name</th>
                    <th>country_id</th>
                    <th>Show answers</th>
                </tr>
                </thead>
                <tbody>
                @foreach($respondents as $respondent)
                    <tr class="border-2">
                        <td>{{ $respondent->id }}</td>
                        <td>{{ $respondent->uuid }}</td>
                        <td>{{ $respondent->user_id }}</td>
                        <td>{{ $respondent->email }}</td>
                        <td>{{ $respondent->city }}</td>
                        <td>{{ $respondent->name }}</td>
                        <td>{{ $respondent->role }}</td>
                        <td>{{ $respondent->company_name }}</td>
                        <td>{{ $respondent->country_id }}</td>
                        <td><a class="relative inline-flex items-center px-16 py-2 border border-blue-400 text-sm
                font-medium rounded-md text-black bg-white hover:bg-gray-50"
                               href="{{ route('respondent-answers', ['respondent'=> $respondent->id]) }}">Show</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="paginate">
                {!! $respondents->links() !!}
            </div>
        </div>
    </div>
</x-app-layout>