<x-app-layout>
    <div class="flex">
        <div class="w-1/6 h-full m-4">
            <p class="mb-4">CIVITAS Handshake</p>
            <div class="grid col-1">
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50 m-1"> Self assesments </a>
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50 m-1"> Database </a>
                <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-white-50 bg-gray-300/50 hover:bg-gray-50 m-1"> Support </a>
            </div>
        </div>
        <div class="w-5/6">
            <div class="ml-6 mt-6 text-blue-900">Respondent answers
                <a href="{{ route('all-respondents') }}" class="text-xs font-medium rounded-md text-blue-400 bg-white">
                    < Back
                    to all Respondent </a>
            </div>
            <div class="m-6 p-5 bg-white">
                <div class="flex p-8">
                    <div class="border-2 rounded-full h-[70px] w-[70px]">
                    </div>
                    <div class="text-xs ml-2">
                        Level of cycling Performance:
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8 pt-10  bg-white">
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Your name</label>
                        <input type="text" value="{{ $respondent->name }}"
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Email</label>
                        <input type="text" value="{{ $respondent->email }}"
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Location</label>
                        <input type="text" value="{{ $respondent->city }}"
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Phone</label>
                        <input type="text" value=""
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Role</label>
                        <input type="text" value="{{ $respondent->role }}"
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                    <div class="mt-1">
                        <label for="name" class="block text-sm font-medium text-warm-gray-900">Company name</label>
                        <input type="text" value="{{ $respondent->company_name }}"
                               class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md">
                    </div>
                </div>
            </div>
            <div class="ml-6 bg-white mt-6 p-4">
                <div class="flex justify-end mr-6">
                    @livewire('create-user', ['respondent' => $respondent])
                    <a href="{{ route('delete-respondent', ['respondent' => $respondent]) }}" class="ml-3 relative inline-flex items-center border border-gray-300
            text-sm font-medium rounded-md text-white-50 bg-orange-400 px-2 hover:bg-gray-50"> Deny </a>
                </div>
                <table>
                    @foreach($groups as $group)
                        <tr>
                            <td class="text-xl p-3 my-4" colspan="2">{{ $group->title }}</td>
                        </tr>
                        <tr>
                            <th>question</th>
                            <th>answer</th>
                        </tr>
                        <tbody class="border-2">
                        @foreach($group->questions as $question)
                            <tr class="border-2 m-4">
                                <td>{{ $question->id}} {{ $question->title }}</td>
                                <td>{{$question->options->first()->value}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
