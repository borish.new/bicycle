<x-app-layout>
    <div class="ml-6 mt-6 text-xl text-blue-900">All users</div>
    <div class="bg-white m-6 p-4">
        <div class="ml-6 mt-6">
            <table class="w-full">
                <thead class="border-2 bg-gray-300/50">
                <tr>
                    <th>id</th>
                    <th class="pl-14">name</th>
                    <th class="pl-24">email</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="paginate">
                {!! $users->links() !!}
            </div>
        </div>
    </div>
</x-app-layout>