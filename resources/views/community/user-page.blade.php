<x-app-layout>
    <div class="">
        <div class="w-[1440px] h-[270px] bg-gray-300 ml-36">
            <div class="flex ml-36">
                <div class="border-2 rounded-full h-[150px] w-[150px] bg-white  mt-12"></div>
                <div class="text-xl w-[762px] h-[120px] ml-5  mt-16">Riga aspires to develop everyday cycling as a
                    mobile, environment–friendly and healthy means of transport
                </div>
            </div>
        </div>

        <div class="mx-auto columns-2 flex flex-row ml-48">
            <div class="m-5">
                <div class="border-2 w-[295px] h-[197px] bg-white">
                    <p class="text-2xl text-center pt-1">Riga</p>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <p class="pl-3 pt-3 text-xs">Level of Cycling Performance:</p>
                </div>

                <div class="w-[295px] h-[363px] bg-white mt-3">
                    <div class="pl-3 pt-3 text-sm">Contact</div>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-24 pt-4">
                        <div class="border-2 rounded-full h-[40px] w-[40px]"></div>
                    </div>
                    <div class="flex justify-center pt-4 flex-col">
                        <div class="text-center text-xs mt-2">
                            Ieva Pastare
                        </div>
                        <div class="text-center text-xs mt-2">
                            Expert in infrastructure
                        </div>
                        <div class="text-center text-xs mt-2">
                            Riga City Council Traffic Department
                        </div>
                        <div class="text-center text-xs mt-2">
                            ieva.pastare@riga.lv
                        </div>
                        <div class="text-center text-xs mt-2">
                            +371 67012766
                        </div>
                    </div>
                </div>

                <div class="w-[295px] h-[741px] bg-white mt-3">
                    <div class="pl-3 pt-3 text-sm">Offering mentorship on</div>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 py-3 text-sm">Planning, Regulations and Standards</div>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Cycling Priorities and
                        Segregated Routes </a>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Land Use Planning and
                        Cycling </a>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 py-3 text-sm">Infrastructure and Services</div>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Cycling Infrastructure
                        Design </a>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Smarter Streets </a>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 py-3 text-sm">Modelling and Assessment</div>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Cycle Modelling </a>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 py-3 text-sm">Awareness and Education</div>
                    <a href="#" class="ml-3 relative inline-flex items-center border border-gray-300
            text-xs font-medium rounded-sm text-blue-600 bg-gray-300/50 hover:bg-gray-50"> Raising the Profile of Safer
                        Streets </a>
                </div>
            </div>

            <div class="">
                <div class="py-6">
                    <div class="text-2xl ">About Riga</div>
                    <div class="text-xs py-3  w-[925px] h-[135px]">The biggest city in the Baltic states, Riga blends
                        timeless tradition and
                        cutting-edge cool. A Nordic blonde with a fiery heart, there is far more to Riga than meets the
                        eye. Its 800 years of turbulent history have seen everyone from German knights to Swedish kings
                        and Soviet commissars leave their mark. Today, Latvia’s capital is an exciting European
                        metropolis at the crossroads of eastern and northern Europe. Riga’s skyline tells its story -
                        timeless Gothic spires in the Old Town mingle with the grand facades of one of the world’s
                        richest collections of Art Nouveau buildings on the city's boulevards.
                    </div>
                </div>

                <div class="flex justify-between w-[925px] h-[135px] bg-blue-700 my-10 rounded-md">
                    <div class="">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <div class=""></div>
                    <div class=""></div>
                    <div class=""></div>
                    <div class=""></div>
                    <div class=""></div>
                </div>

                <div class="bg-white w-[925px] h-[215px] my-8">
                    <div class="pl-3 pt-3 text-sm">
                        Main cycling challenges
                    </div>
                    <div class="w-[259px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 pt-3 text-xs">
                        Riga has no deep-seated cycling tradition, and most people do not use the bike as their everyday
                        mode of transport. An understanding of the link between cycling and green and healthy living is
                        increasing annually by 10% among inhabitants. The greatest challenge is addressing a lack of
                        good quality, well-connected, and safe cycling infrastructure. Awareness raising is necessary
                        among all road users - drivers, bicyclists, pedestrians and public transport users - to
                        communicate the shared principles of mutual understanding and a safe road movement.
                    </div>
                </div>

                <div class="flex justify-between w-[925px] h-[210px] my-10 rounded-md">
                    <div class="bg-white m-1 text-xs w-1/6 pt-[150px]">
                        GDP Gross domestic product
                    </div>
                    <div class="bg-white m-1 text-xs w-1/6 pt-[150px]">
                        Students are living and studying
                    </div>
                    <div class="bg-white m-1 text-xs w-1/6 pt-[130px]">
                        Level of car ownership per 1.000 hab
                    </div>
                    <div class="bg-white m-1 text-xs w-1/6 pt-[150px]">
                        City spend on cycling investment
                    </div>
                    <div class="bg-white m-1 text-xs w-1/6 pt-[150px]">
                        Сycling modal share
                    </div>
                </div>

                <div class="bg-white w-[925px] h-[441px] my-8">
                    <div class="pl-3 pt-3 text-sm">
                        Infrastructure and facilities
                    </div>
                    <div class="w-[890px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="pl-3 pt-3 text-xs text-blue-600">
                        Regional Cycling Networks
                    </div>
                    <div class="pl-3 pt-3 text-xs">
                        Some parts of the network well-connected locally, but investment needed to achieve coherence
                        across the entire area and a consistent quality of infrastructure and signage
                    </div>
                    <div class="pl-3 pt-3 text-xs text-blue-600">
                        Regional Cycling Networks
                    </div>
                    <div class="pl-3 pt-3 text-xs">
                        Some parts of the network well-connected locally, but investment needed to achieve coherence
                        across the entire area and a consistent quality of infrastructure and signage
                    </div>
                    <div class="pl-3 pt-3 text-xs text-blue-600">
                        Regional Cycling Networks
                    </div>
                    <div class="pl-3 pt-3 text-xs">
                        Some parts of the network well-connected locally, but investment needed to achieve coherence
                        across the entire area and a consistent quality of infrastructure and signage
                    </div>
                    <div class="w-[890px] h-[2px] bg-gray-300 ml-3 mt-3"></div>
                    <div class="text-blue-600 text-center text-sm pt-3">
                        View All
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>