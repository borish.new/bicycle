<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CommunityController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Survey\SurveyController;
use App\Http\Livewire\Home\Home;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Public routes without middlewares
|--------------------------------------------------------------------------
*/
Route::get('/', Home::class)->name('home');

Route::get('/self-assessment/introduction/{step}', [SurveyController::class, 'introduction'])->name('steps');
Route::get('/self-assessment/respondent/{respondent}/question/{question}', [SurveyController::class, 'question'])->name('question');
Route::get('/self-assessment/respondent/{respondent:uuid}/group/{group}', [SurveyController::class, 'group'])->name('group');

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('/respondents', [AdminController::class, 'indexAllRespondents'])->name('all-respondents');
    Route::get('/users', [AdminController::class, 'indexUsers'])->name('users');
    Route::get('/respondent/answer/{respondent}', [AdminController::class, 'showAnswers'])->name('respondent-answers');
    //Route::get('/respondents/answer/{respondent}/create-user', [AdminController::class, 'createUser'])->name('create-user');
    Route::get('/respondents/answer/{respondent}/delete', [AdminController::class, 'deleteRespondent'])->name('delete-respondent');
});

Route::get('/self-assessment/respondent/{respondent:uuid}/results', [SurveyController::class, 'results'])->name('results');

Route::get('/community/user-page', [CommunityController::class, 'index'])->name('user-page');
/*
|--------------------------------------------------------------------------
| Routes protected with auth middlewares
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->group(function(){
    /*
    |--------------------------------------------------------------------------
    | Routes for dashboard
    |--------------------------------------------------------------------------
    */
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
});
/*
|--------------------------------------------------------------------------
| Routes for auth
|--------------------------------------------------------------------------
*/
require __DIR__.'/auth.php';
