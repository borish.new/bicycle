<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Respondent extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'uuid',
        'user_id',
        'email',
        'city',
        'name',
        'role',
        'company_name',
        'country_id',
    ];

    public function answer(): HasMany
    {
        return $this->hasMany(RespondentResponse::class);
    }
}
