<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RespondentResponse extends Model
{
    use HasFactory;

    protected $table = 'respondent_question_responses';

    protected $fillable = [
        'id',
        'respondent_id',
        'question_option_id',
        'value',
    ];

    protected $with = ['option'];

    public function option(): BelongsTo
    {
        return $this->belongsTo(QuestionOption::class, 'question_option_id', 'id');
    }

    public function respondent(): BelongsTo
    {
        return $this->belongsTo(Respondent::class);
    }
}
