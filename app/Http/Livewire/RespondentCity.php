<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RespondentCity extends Component
{
    public string $city = '';

    protected $rules = [
        'city' => 'required',
    ];

    public function render()
    {
        return view('livewire.respondent-city');
    }

    public function nextStep()
    {
        $this->validate();
        return redirect()->route('steps', ['step' => 7, 'city' => $this->city]);
    }
}
