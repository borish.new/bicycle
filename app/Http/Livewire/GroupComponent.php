<?php

namespace App\Http\Livewire;

use App\Models\QuestionGroup;
use App\Models\Respondent;
use Livewire\Component;

class GroupComponent extends Component
{
    public Respondent $respondent;
    public $group_id = '';

    public function render()
    {
        return view('livewire.group-component');
    }

    public function goToPreviousQuestion()
    {
        if ($this->group_id == 1) {
            return redirect()->route('steps', ['step' => 7, 'respondent' => $this->respondent->uuid]);
        }

        $previousGroupLatestQuestion = $this->getPreviousGroupLastQuestion($this->group_id);

        if ($previousGroupLatestQuestion) {
            return redirect()->route('question', [
                'respondent' => $this->respondent->uuid,
                'question' => $previousGroupLatestQuestion->id,
            ]);
        }

        return redirect()->to('/');
    }

    public function getPreviousGroupLastQuestion($currentGroupId)
    {
        $previousGroup = QuestionGroup::where('id', '<', $currentGroupId)->orderBy('id', 'DESC')->first();
        return $previousGroup->questions()->latest('id')->first();
    }

    public function continueSurvey()
    {
        $group = QuestionGroup::with('questions')->find($this->group_id);
        $question = $group->questions->first();
        return redirect()->route('question', ['question' => $question->id, 'respondent' => $this->respondent->uuid]);
    }
}
