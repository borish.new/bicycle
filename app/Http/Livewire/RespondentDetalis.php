<?php

namespace App\Http\Livewire;

use App\Models\Country;
use App\Models\QuestionGroup;
use App\Models\Respondent;
use Livewire\Component;

class RespondentDetalis extends Component
{
    public ?Respondent $respondent;
    public string $city = '';
    public string $email = '';
    public string $name = '';
    public string $role = '';
    public string $company_name = '';
    public string $country_id = '';

    public $countries;

    protected $rules = [
        'city' => 'required',
        'email' => 'required',
        'name' => 'required',
        'role' => 'required',
        'company_name' => 'required',
        'country_id' => 'required',
    ];

    public function mount()
    {
        if ($this->respondent) {
            $this->city = $this->respondent->city ?? $this->city ?? '';
            $this->email = $this->respondent->email ?? '';
            $this->name = $this->respondent->name ?? '';
            $this->role = $this->respondent->role ?? '';
            $this->company_name = $this->respondent->company_name ?? '';
            $this->country_id = $this->respondent->country_id ?? '';
        }

        $this->countries = Country::get(['id', 'countryName']);
    }

    public function render()
    {
        return view('livewire.respondent-detalis');
    }

    public function createRespondent()
    {
        $this->validate();

        return Respondent::updateOrCreate(['email' => $this->email], [
            'uuid' => md5($this->email . time()),
            'email' => $this->email,
            'city' => $this->city,
            'name' => $this->name,
            'role' => $this->role,
            'company_name' => $this->company_name,
            'country_id' => $this->country_id,
        ]);
    }

    public function createAndContinue(QuestionGroup $group)
    {
        $respondent = $this->createRespondent();

        $answer = $respondent->answer()->with('option')->latest()->first();

        if ($answer) {
            return redirect()->route('question', [
                'respondent' => $respondent->uuid,
                'question' => $answer->option->question_id,
            ]);
        }

        return redirect()->route('group', ['group' => 1, 'respondent' => $respondent->uuid]);
    }

    public function saveAndContinueLater()
    {
        $respondent = $this->createRespondent();
        return redirect()->to('/');
    }
}
