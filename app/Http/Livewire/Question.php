<?php

namespace App\Http\Livewire;

use App\Models\Question as QuestionModel;
use App\Models\QuestionGroup;
use App\Models\Respondent;
use Livewire\Component;

class Question extends Component
{
    public Respondent $respondent;
    public QuestionModel $question;

    public $answer = '';

    protected $rules = [
        'answer' => 'required',
    ];

    public function mount()
    {
        $this->question->load('options');

        $this->answer = $this->respondent->answer()->whereHas('option', function ($query) {
            $query->where('question_id', $this->question->id);
        })->latest('created_at')->first()->question_option_id ?? '';
    }

    public function render()
    {
        return view('livewire.question');
    }

    public function saveAnswerAndGoToNext()
    {
        $this->validate();

        $this->respondent->answer()->updateOrcreate([
            'question_option_id' => $this->answer,
        ]);

        $nextQuestion = $this->getNextQuestion();

        if ($nextQuestion) {
            return redirect()->route('question', [
                'respondent' => $this->respondent->uuid,
                'question' => $nextQuestion->id,
            ]);
        }

        $nextGroup = $this->getNextGroup();

        if ($nextGroup) {
            return redirect()->route('group', [
                'respondent' => $this->respondent->uuid,
                'group' => $nextGroup->id,
            ]);
        }

        return redirect()->route('results', [
            'respondent' => $this->respondent->uuid,
        ]);
    }

    public function getNextQuestion()
    {
        return QuestionModel::where('id', '>', $this->question->id)->where('question_group_id', $this->question->question_group_id)->orderBy('id')->first();
    }

    public function getPreviousQuestion()
    {
        return QuestionModel::where('id', '<', $this->question->id)->where('question_group_id', $this->question->question_group_id)->orderBy('id', 'DESC')->first();
    }

    public function goToPreviousQuestion()
    {
        $previousQuestion = $this->getPreviousQuestion();

        if ($previousQuestion) {
            return redirect()->route('question', [
                'respondent' => $this->respondent->uuid,
                'question' => $previousQuestion->id,
            ]);
        }

        return redirect()->route('group', [
            'respondent' => $this->respondent->uuid,
            'group' => $this->question->question_group_id,
        ]);
    }

    public function saveAnswerAndContinueLater()
    {
        $this->respondent->answer()->create([
            'question_option_id' => $this->answer,
        ]);

        return redirect()->to('/');
    }

    public function getNextGroup()
    {
        return QuestionGroup::where('id', '>', $this->question->question_group_id)->orderBy('id')->first();
    }
}
