<?php

namespace App\Http\Livewire;

use App\Mail\UserEmail;
use App\Models\Respondent;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class CreateUser extends Component
{
    public Respondent $respondent;

    public $user = '';

    public function render()
    {
        return view('livewire.create-user');
    }

    public function createUser()
    {
        $password = substr(md5(time()), 0, 8);

        $user = User::create([
            'name' => $this->respondent->name,
            'email' => $this->respondent->email,
            'password' => Hash::make($password),
        ]);

        $this->respondent->update(['user_id' => $user->id]);

        Mail::to($user->email)->send(new UserEmail($user->email, $password));

        return redirect()->route('users');
    }
}
