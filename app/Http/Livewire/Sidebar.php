<?php

namespace App\Http\Livewire;

use App\Models\QuestionGroup;
use Livewire\Component;

class Sidebar extends Component
{
    public string $step;
    public $groups;

    public function mount()
    {
        $this->groups = QuestionGroup::get();
    }

    public function render()
    {
        return view('livewire.sidebar');
    }
}
