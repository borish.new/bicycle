<?php

namespace App\Http\Controllers\Survey;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\QuestionGroup;
use App\Models\Respondent;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function introduction(Request $request, $step)
    {
        $respondent = Respondent::whereUuid($request->respondent)->first();

        return view('survey.introduction.step-' . $step, [
            'step' => $step,
            'city' => $request->input('city'),
            'respondent' => $respondent,
        ]);
    }

    public function question(string $respondent, Question $question)
    {
        $respondent = Respondent::whereUuid($respondent)->first();

        if (!$respondent) {
            abort(404);
        }

        return view('survey.question.question', ['respondent' => $respondent, 'question' => $question]);
    }

    public function group(Respondent $respondent, QuestionGroup $group)
    {
        return view('survey.group.group', ['group' => $group, 'respondent' => $respondent]);
    }

    public function results(Respondent $respondent)
    {
        $group = QuestionGroup::latest('id')->first();
        $question = $group->questions()->latest('id')->first();

        $previousPage = route('question', [
            'respondent' => $respondent->uuid,
            'question' => $question->id,
        ]);

        return view('survey.group.your-result', ['previousPage' => $previousPage]);
    }
}
