<?php

namespace App\Http\Controllers;

use App\Models\QuestionGroup;
use App\Models\Respondent;
use App\Models\User;

class AdminController extends Controller
{
    public function indexAllRespondents()
    {
        $respondents = Respondent::paginate(10);
        return view('admin.respondents', ['respondents' => $respondents]);
    }

    public function showAnswers(Respondent $respondent)
    {
        $groups = QuestionGroup::with(['questions' => function ($query) use ($respondent) {
            $query->whereHas('options', function ($query) use ($respondent) {
                $query->whereHas('respondent', function ($query) use ($respondent) {
                    $query->where('respondents.id', $respondent->id);
                });
            });
            $query->with('options', function ($query) use ($respondent) {
                $query->latest('question_options.id');
                $query->whereHas('respondent', function ($query) use ($respondent) {
                    $query->where('respondents.id', $respondent->id);
                });
            });
        }])->get();
        return view('admin.respondent-answers', ['respondent' => $respondent, 'groups' => $groups]);
    }

    public function indexUsers()
    {
        $users = User::paginate(10);
        return view('admin.users', ['users' => $users]);
    }

    public function deleteRespondent(Respondent $respondent)
    {
        $respondent->delete();
        return redirect()->route('all-respondents')->with('success', 'Respondent delete!');
    }
}
